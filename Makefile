python-lib:
	sudo apt install python3

pip-lib:
	sudo apt install python3-pip 

tk-lib:
	sudo apt install python3-tk

pygame-lib:
	pip install pygame

dependencies:
	make python-lib
	make pip-lib
	make tk-lib
	make pygame-lib

run:
	python3 main.py
