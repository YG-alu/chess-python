from dependencies import check_report
check_report()

from tkinter import *
from tkinter import messagebox

from os import system

import pygame

pygame.init()

image_dir = f"./images"

white = (255, 255, 255)


def setup():
    global window
    window = pygame.display.set_mode((655, 750))
    pygame.display.set_caption("Chess")
    window.fill(white)
    board1 = pygame.image.load(f"{image_dir}/ChessBoard.png")
    board2 = pygame.image.load(f"{image_dir}/ChessBoard2.png")
    board2 = pygame.transform.scale(board2, (650, 650))
    window.blit(board1, (0, 0))
    pygame.display.update()


def message_box(title, message):
    root = Tk()
    root.attributes("-topmost", True)
    root.withdraw()
    if messagebox.askokcancel(title, message):
        return True
    else:
        return False


class Images:
    def __init__(self, display=False):
        self.window = window
        if display:
            self.display_figures()

    def import_images(self):
        figures = [[pygame.image.load(f"{image_dir}/white/white_pawn.png"),
                    pygame.image.load(f"{image_dir}/white/white_pawn.png"),
                    pygame.image.load(f"{image_dir}/white/white_pawn.png"),
                    pygame.image.load(f"{image_dir}/white/white_pawn.png"),
                    pygame.image.load(f"{image_dir}/white/white_pawn.png"),
                    pygame.image.load(f"{image_dir}/white/white_pawn.png"),
                    pygame.image.load(f"{image_dir}/white/white_pawn.png"),
                    pygame.image.load(f"{image_dir}/white/white_pawn.png"),
                    pygame.image.load(f"{image_dir}/white/white_rook.png"),
                    pygame.image.load(f"{image_dir}/white/white_knight.png"),
                    pygame.image.load(f"{image_dir}/white/white_bishop.png"),
                    pygame.image.load(f"{image_dir}/white/white_queen.png"),
                    pygame.image.load(f"{image_dir}/white/white_king.png"),
                    pygame.image.load(f"{image_dir}/white/white_bishop.png"),
                    pygame.image.load(f"{image_dir}/white/white_knight.png"),
                    pygame.image.load(f"{image_dir}/white/white_rook.png")], [
                       pygame.image.load(f"{image_dir}/black/black_pawn.png"),
                       pygame.image.load(f"{image_dir}/black/black_pawn.png"),
                       pygame.image.load(f"{image_dir}/black/black_pawn.png"),
                       pygame.image.load(f"{image_dir}/black/black_pawn.png"),
                       pygame.image.load(f"{image_dir}/black/black_pawn.png"),
                       pygame.image.load(f"{image_dir}/black/black_pawn.png"),
                       pygame.image.load(f"{image_dir}/black/black_pawn.png"),
                       pygame.image.load(f"{image_dir}/black/black_pawn.png"),
                       pygame.image.load(f"{image_dir}/black/black_rook.png"),
                       pygame.image.load(f"{image_dir}/black/black_knight.png"),
                       pygame.image.load(f"{image_dir}/black/black_bishop.png"),
                       pygame.image.load(f"{image_dir}/black/black_king.png"),
                       pygame.image.load(f"{image_dir}/black/black_queen.png"),
                       pygame.image.load(f"{image_dir}/black/black_bishop.png"),
                       pygame.image.load(f"{image_dir}/black/black_knight.png"),
                       pygame.image.load(f"{image_dir}/black/black_rook.png")]]
        return figures

    def display_figures(self):
        img = self.import_images()

        for i in range(2):
            for j in range(8):
                img[i][j] = pygame.transform.scale(img[i][j], (70, 70))
                img[i][j+8] = pygame.transform.scale(img[i][j+8], (70, 70))

                if i == 0:
                    self.window.blit(img[i][j], (j * 77 + 35, 495))
                    self.window.blit(img[i][j + 8], (j * 77 + 35, 575))
                else:
                    self.window.blit(img[i][j], (j * 77 + 35, 115))
                    self.window.blit(img[i][j + 8], (j * 77 + 35, 35))

        pygame.display.update()

    def add_rects(self):
        board_rect = [pygame.draw.rect(window, white, (i * 77 + 35, i, 70, 70)) for i in range(64)]
        pygame.display.update()

    def move(self, move_pos):
        pass


def collide(X, Y, rect):
    return (X >= rect.x-rect.width/2 and
            X <= rect.x+rect.width/2 and
            Y >= rect.y-rect.height/2 and
            Y <= rect.y+rect.height/2)


def fig_click(position):
    for i in range(2):
        for j in range(16):
            if collide(position[0], position[1], Images().import_images()[i][j].get_rect()):
                return [i, j]
            else:
                continue

    return None

def board_click(position):
    pass


def main():
    setup()
    images = Images(display=True)
    run = True
    exit_loop = False
    while run:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False

            if event.type == pygame.MOUSEBUTTONUP and event.button == 1:
                pos = pygame.mouse.get_pos()
                clicked = fig_click(pos)

                if clicked is None:
                    continue
                else:
                    pygame.draw.rect(window, (255, 0, 0), fig[clicked[0]][clicked[1]].get_rect())
                    pygame.display.update()

                    wait_for_click = True
                    while wait_for_click:
                        for event2 in pygame.event.get():
                            if event2.type == pygame.MOUSEBUTTONUP and event.button == 1:
                                pos2 = pygame.mouse.get_pos()
                                clicked2 = board_click(pos)

                                if clicked2 is None:
                                    wait_for_click = False
                                else:
                                    

                                    wait_for_click = False

                            if event2.type == pygame.QUIT:
                                exit_loop = True
                                wait_for_click = False
                                

        pygame.display.update()
        if exit_loop:
            run = False


    if message_box("Confirmed exit", "Are you confirming to exit"):
        pygame.quit()
    else:
        run = True


if __name__ == '__main__':
    main()
