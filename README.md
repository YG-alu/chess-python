# Chess
To run this chess game, run the following command:
```
$ make run
```
or:
```
$ python3 main.py
```

# Depenecies
The dependecies you will need will be:
- python3
- pygame
- tkinter

# Installing python3
To install python3 on a Debian base os, run:
```
# apt install python3
```
On Windows go to this [site](https://www.python.org/).
On Mac:
```
$ brew install python
```

To install them you can run the command:
```
$ make dependencies
```
