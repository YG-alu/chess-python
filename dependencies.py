def check_report():
	try:
		import pygame
		print("pygame is ready for use")
	except:
		if input("pygame isn't installed yet. Do you want to install it now? [y/n]") == "y":
			try:
				import os
				exe = os.system("make pygame-lib")
				if exe == 1:
					if input("pip isn't installed yet. Do you want to install it now? [y/n]") == "y":
						os.system("make pip-lib")
					else:
						print("Install it with 'sudo apt install python3-pip'")
				else:
					print("pygame is ready for use")
			except:
				print("You need to install manually with 'pip install pygame'\nIf you don't have pip, install it with 'sudo apt install python3-pip'")
		
		else:
			print("You need to install manually with 'sudo apt install python3-pip', then 'pip install pygame'")
			exit(0)

	try:
		import tkinter
		print("tkinter is ready for use")
	except:
		if input("tkinter isn't installed yet. Do you want to install it now? [y/n]") == "y":
			try:
				import os
				exe = os.system("make tk-lib")
				if exe == 1:
					if input("pip isn't installed yet. Do you want to install it now? [y/n]") == "y":
						os.system("make pip-lib")
					else:
						print("Install it with 'sudo apt install python3-pip'")
				else:
					print("tkinter is ready for use")
			except:
				print("You need to install manually!")
		
		else:
			print("You need to install manually with 'sudo apt install python3-tk'")
			exit(0)

if __name__ == '__main__':
	check_report()
